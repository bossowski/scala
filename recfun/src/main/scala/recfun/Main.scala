package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || r == c)
      1
    else
      pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {

    def myBalance(count: Int, chars: List[Char]): Boolean = {
      if (count < 0)
        return false

      if (chars.isEmpty)
        if (count == 0)
          return true
        else return false

      if (chars.head == '(')
        return myBalance(count + 1, chars.tail)
      else if (chars.head == ')')
        return myBalance(count - 1, chars.tail)

      myBalance(count, chars.tail)
    }

    myBalance(0, chars)
  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {
    if (money == 0)
      return 0

    if (coins.isEmpty)
      return 0

    def total(n: Int, coins: List[Int]): Int = {
      if (n < 0)
        return 0

      if (n == 0)
        return 1

      if (coins.isEmpty) {
        return 0
      }

      total(n - coins.head, coins) + total(n, coins.tail)
    }

    total(money, coins)
  }
}
